module.exports = {
	type: 'icicle',

	range: 9,

	statType: 'int',
	statMult: 1,
    
    needLos: true,

	damage: 10,

	row: 6,
	col: 4,
    spriteSheet: 'attacks',
    
    particles: {
		color: {
			start: ['c0c3cf', '929398'],
			end: ['929398', 'c0c3cf']
		},
		scale: {
			start: {
				min: 4,
				max: 10
			},
			end: {
				min: 0,
				max: 4
			}
		},
		speed: {
			start: {
				min: 2,
				max: 16
			},
			end: {
				min: 0,
				max: 8
			}
		},
		lifetime: {
			min: 1,
			max: 1
		},
		spawnType: 'circle',
		spawnCircle: {
			x: 0,
			y: 0,
			r: 12
		},
		randomScale: true,
		randomSpeed: true,
		chance: 0.075,
		randomColor: true
	},

	cast: function (action) {
        let obj = this.obj;

		let physics = obj.instance.physics;
        let syncer = obj.instance.syncer;

		let target = action.target;

		let path = this.getPath(physics, this.obj.x, this.obj.y, target.x, target.y);

        if (path.length < 1) {
            return;
        }

        let lastTile = path[path.length - 1];

        let effect = {
            x: lastTile[0],
            y: lastTile[1],
            components: [{
                type: 'particles',
                noExplosion: true,
                ttl: path.length,
                blueprint: this.particles
            }]
        };

        syncer.queue('onGetObject', effect, -1);

        for (let i = 0; i < path.length; i++) {
            this.queueCallback(this.onWarningOver.bind(this, path[i][0], path[i][1]), i * consts.tickTime);
        }

		this.sendBump(target);

		return true;
    },
    
    getPath: function (physics, fromX, fromY, toX, toY) {
		if ((fromX < 0) || (fromY < 0) || (fromX >= physics.width) | (fromY >= physics.height) || (toX < 0) || (toY < 0) || (toX >= physics.width) | (toY >= physics.height))
			return [];

		let graphGrid = physics.graph.grid;

		if ((!graphGrid[fromX][fromY]) || (!graphGrid[toX][toY]))
			return [];

		let dx = toX - fromX;
		let dy = toY - fromY;

		let distance = Math.sqrt((dx * dx) + (dy * dy));

		dx /= distance;
		dy /= distance;

		fromX += 0.5;
		fromY += 0.5;

		distance = Math.ceil(distance);

		let x = 0;
        let y = 0;
        
        let path = [];

		for (let i = 0; i < distance; i++) {
			fromX += dx;
			fromY += dy;

			x = ~~fromX;
            y = ~~fromY;
            
			let node = graphGrid[x][y];

            if ((!node) || (node.weight === 0))
                return path;
            else {
                path.push([x, y]);

                if ((x === toX) && (y === toY))
                    return path;
            }
		}

		return path;
	},

	onWarningOver: function (x, y) {
		const { obj, spriteSheet, rowOptions, col, row } = this;

		let physics = obj.instance.physics;
		let syncer = obj.instance.syncer;

		const useRow = (row !== null) ? row : rowOptions[~~(Math.random() * rowOptions.length)];

		let effect = {
			x: x,
			y: y,
			components: [{
				type: 'attackAnimation',
				destroyObject: true,
				row: useRow,
				col,
				frameDelay: 4 + ~~(Math.random() * 7),
				spriteSheet
			}]
		};

		syncer.queue('onGetObject', effect, -1);

		let mobs = physics.getCell(x, y);
		let mLen = mobs.length;
		for (let k = 0; k < mLen; k++) {
			let m = mobs[k];

			//Maybe we killed something?
			if (!m) {
				mLen--;
				continue;
			} else if (!m.aggro)
				continue;
			else if (!this.obj.aggro.canAttack(m))
				continue;

			let damage = this.getDamage(m);
			m.stats.takeDamage(damage, 1, obj);
		}
	}
};
