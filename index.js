module.exports = {
	name: 'Icicle Spell',

	extraScripts: [
		'spells/spellIcicle',
	],

	init: function () {
		this.events.on('onBeforeGetSpellsInfo', this.beforeGetSpellsInfo.bind(this));
		this.events.on('onBeforeGetSpellsConfig', this.beforeGetSpellsConfig.bind(this));
		this.events.on('onBeforeGetSpellTemplate', this.beforeGetSpellTemplate.bind(this));
		this.events.on('onBeforeGetClientConfig', this.onBeforeGetClientConfig.bind(this));
		this.events.on('onAfterGetZone', this.onAfterGetZone.bind(this));
	},

	onAfterGetZone: function (zone, config) {
		if (zone !== 'fjolarok')
			return;

		let newRunes = [{
			generate: true,
			spell: true,
			quality: 0,
			infinite: true,
			spellName: 'icicle',
			worth: 3
		}];

		let asvaldTrade = config.mobs.asvald.properties.cpnTrade;
		Array.prototype.push.apply(asvaldTrade.items.extra, newRunes);
	},

	onBeforeGetClientConfig: function ({ resourceList, textureList }) {
		resourceList.push(`${this.folderName}/images/abilityIcons.png`);
	},

	beforeGetSpellTemplate: function (spell) {
		if (spell.type === 'Icicle')
			spell.template = require('./spells/spellIcicle');
	},

	beforeGetSpellsConfig: function (spells) {
		spells['icicle'] = {
			statType: ['int'],
			statMult: 1,
			cdMax: 7,
			castTimeMax: 2,
            manaCost: 5,
            element: 'frost',
			range: 9,
			random: {
				damage: [4, 32]
			}
		};
	},

	beforeGetSpellsInfo: function (spells) {
		spells.push({
			name: 'Icicle',
			description: 'Fires a an icy burst at your target, damaging enemies along the way.',
			type: 'icicle',
			icon: [0, 0],
			animation: 'raiseHands',
			spritesheet: `${this.folderName}/images/abilityIcons.png`//,
			// particles: {
			// 	color: {
			// 		start: ['ff4252', 'b34b3a'],
			// 		end: ['b34b3a', 'ff4252']
			// 	},
			// 	scale: {
			// 		start: {
			// 			min: 2,
			// 			max: 14
			// 		},
			// 		end: {
			// 			min: 0,
			// 			max: 8
			// 		}
			// 	},
			// 	lifetime: {
			// 		min: 1,
			// 		max: 3
			// 	},
			// 	alpha: {
			// 		start: 0.7,
			// 		end: 0
			// 	},
			// 	randomScale: true,
			// 	randomColor: true,
			// 	chance: 0.6
			// }
		});
	}
};
